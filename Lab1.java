import java.lang.String;
import java.util.Scanner;
public class Lab1 {

    public static float[] FirstFunc(float r1, float r2) {
        float pi = 3.14f;
        float s1,s2,s3;
        s1 = pi * r1 * r1;
        s2 = pi * r2 * r2;
        s3 = s1 - s2;
        return new float[]{s1,s2,s3};
    }

    public static int Integer(int number) {
        int ostacha, temp_number1;
        ostacha = number % 100;
        temp_number1 = (number - ostacha) / 100;
        ostacha = ostacha * 10 + temp_number1;
        return ostacha;
    }

    public static boolean Boolean(int a, int b, int c) {
        boolean isTrue = true;
        if (a > 0) {
            return isTrue;
        } else if (b > 0) {
            return isTrue;
        } else if (c > 0) {
            return isTrue;
        } else {
            isTrue = false;
            return isTrue;
        }
    }

    public static int If(int a, int b, int c) {
        if ((a > b && a < c) || (a > c && a < b)) {
            return a;
        }
        if ((b > a && b < c) || (b > c && b < a)) {
            return b;
        }
        if ((c > a && c < b) || (c > b && c < a)) {
            return c;
        }
        return a;
    }
    public static float Case(int c,float a)
    {
        float kor_z_dwox = 1.41f, c_1 = a*kor_z_dwox, h = c_1/2f, s = c_1*(h/2f);
        switch (c)
        {
            case 1:
                return a;
            case 2:
                return c_1;
            case 3:
                return h;
            case 4:
                return s;
        }
        return c;
    }

    public static float For(float n)
    {
        float sum = 0;
        float k = 1.1f;
        for ( int i = 2; i < n+2; i++ ) {
            sum += k*Math.pow(-1d,i);
            k += 0.1f;
        }
        return sum;
    }
    public static float[] While(float a)
    {
        float sum = 0;
        float k = 0;
        while ( sum <= a )
        {
            k++;
            sum+=1/k;
        }
        return new float[]{k,sum};
    }

    /**
     *
     * @param Arr
     * @return array Rez
     */
    public static int[] Array(int Arr[])
    {
        int Rez[] = new int [Arr.length];
        int count = 1;
        for ( int i = 0; i < Arr.length-1; i++ )
        {
            if ( Arr[i] > Arr[i+1] )
            {
                Rez[count] = i;
                count++;
            }
        }
        Rez[0] = count-1;
        return Rez;
    }

    /**
     *
     * @param Mtrx
     * @return GetMatrix
     */
    public static int[][] Matrix(int Mtrx[][])
    {
        int GetMatrix[][] = new int[Mtrx.length][Mtrx.length];
        for (int j = 0; j < Mtrx.length; j++)
        {
            for (int i = 0; i < Mtrx.length; i++)
            {
                GetMatrix[Mtrx.length-i-1][j] = Mtrx[i][j];
            }
        }
        return GetMatrix;
    }
}

