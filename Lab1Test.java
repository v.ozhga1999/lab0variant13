import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.util.Arrays;

import static org.testng.Assert.*;

public class Lab1Test {

    @Test
    public void FirstFuncTest()
    {
        float[] rez = Lab1.FirstFunc(6,5);
        assertEquals(rez[0],113.04,0.01);
        assertEquals(rez[1],78.5,0.01);
        assertEquals(rez[2],34.54,0.01);
    }
    @Test
    public void IntegerTest()
    {
        assertEquals(Lab1.Integer (154),541);
    }
    @Test
    public void BooleanTest()
    {
        assertEquals(Lab1.Boolean(3,2,4),true);
    }
    @Test
    public void IfTest()
    {
        assertEquals(Lab1.If(3,2,4), 3);
    }
    @Test
    public void CaseTest()
    {
        assertEquals(Lab1.Case(1,4f),4f);
    }
    @Test
    public void ForTest()
    {
        assertEquals(Lab1.For(5f),1.3f, 0.01);
        assertEquals(Lab1.For(6f),-0.3f, 0.01);
    }
    @Test
    public void WhileTest()
    {
        float[] rez1 = Lab1.While(1.5f);
        assertEquals(rez1[0],3f);
        assertEquals(rez1[1],1.8333f,0.0001);
        float[] rez2 = Lab1.While(4f);
        assertEquals(rez2[0],31f);
        assertEquals(rez2[1],4.0272f,0.0001);
        float[] rez3 = Lab1.While(2f);
        assertEquals(rez3[0],4f);
        assertEquals(rez3[1],2.0833f,0.0001);
    }
    @Test
    public void ArrayTest()
    {
        int[] set = new int[]{3,1,2,5,4,1,2,8,9,7};
        int[] get = {4,0,3,4,8,0,0,0,0,0};
        int[] fget = Lab1.Array(set);
        for ( int i = 0; i < fget.length; ++i )
        {
            assertEquals(get[i],fget[i]);
        }
    }
    @Test
    public void MatrixTest()
    {
        int[][] set = new int[][]{{1,2,3},{4,5,6},{7,8,9}};
        int[][] get = {{7,8,9},{4,5,6},{1,2,3}};
        int[][] fget = Lab1.Matrix(set);
        for(int i = 0; i < set.length; ++i)
        {
            for(int j = 0; j < set.length; ++j)
            {
                assertEquals(get[i][j],fget[i][j]);
            }
        }
    }
}